FN="/etc/profile.d/custom.sh"
NAME='mikew'
HOME="/home/$NAME"
PASSWORD="$1"
PWD=`pwd`
AKEYS="$HOME/.ssh/authorized_keys"

if [ ! -d "/etc/profile.d" ]; then
	FN='~/.bash_profile'
fi

eval FN="$FN"
if [ ! -f "$FN" ]; then
	touch "$FN"
	chmod +x "$FN"
fi

EXISTS=`cat "$FN" | grep "alias rm"`;

if [ -z "$EXISTS" ]; then
    echo "Setting up aliases...."

    echo "alias rm='rm -i'" | tee -a "$FN"
    echo "alias cp='cp -i'" | tee -a "$FN"
    echo "alias mv='mv -i'" | tee -a "$FN"
    echo "alias ls='ls -GFh --color'" | tee -a "$FN"
    echo "alias h='history'" | tee -a "$FN"

    echo 'export PS1="\[\033[36m\]\u\[\033[m\]@\[\033[32m\]\h:\[\033[33;1m\]\w\[\033[m\]\$ "' | tee -a "$FN"
    echo "export CLICOLOR=1" | tee -a "$FN"
    echo "export LSCOLORS=ExFxBxDxCxegedabagacad" | tee -a "$FN"

    echo "shopt -s histappend" | tee -a "$FN"
fi

yum update -y
yum install wget vim unzip lynx iptables-services yum-utils git cvs rdist -y
yum install yum-utils device-mapper-persistent-data lvm2 -y

yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum install docker-ce -y
# yum groupinstall "development tools" -y

systemctl disable firewalld
systemctl stop firewalld
yum remove -y firewalld
systemctl start docker
systemctl enable docker

EXISTS=`cat /etc/passwd | grep $NAME`;
if [ -z "$EXISTS" ]; then
    echo "Adding $NAME..."
    useradd $NAME -d /home/$NAME

    echo $PASSWORD | passwd $NAME --stdin
    usermod -aG adm $NAME
    usermod -aG wheel $NAME 
fi

if [ ! -f "$AKEYS" ]; then
    mkdir "$HOME/.ssh"
    echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC5mQzgYomcF0ABjEk7yVksX45VChg4ncUnGfNSKAfPQf+wkEVi0hEtc3sxLAHw5ZdRr7iAWLhJ7QfQvTN40if89VgbMpgiWoRl2K1CW7fF6SzK7tTHUwIcy1eNq3rL9efrLLC/tKrtTkPTKJJOHOs2eBm2rxdXnj9lVbrSmCVHoiFtqZJWsfSDY6VLZJ/Z8Rcq/yYFmEMosZBY+2r0Vv2Snx1w577pvde+Q+hWxCeIg5yR0oCBSu8oUZ2e74Eoop6X99U+YViq8b19hrTCD+jGuysfCJuO1vt7fAeg6RqijgiakKKILbikr3vZiOKAk0diiNc5CymUMqOGLcme5fYT michael@Patris-MBP.local" > "$AKEYS"
    chmod 755 "$HOME/.ssh"
    chmod 644 "$AKEYS"
fi

usermod -aG docker $NAME

curl -L "https://github.com/docker/compose/releases/download/1.25.0-rc2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

su - $NAME

cd $HOME;

if [ -f ".vimrc" ]; then
    VIMTEST=`cat .vimrc | grep elflord`
fi

if [ -z "$VIMTEST" ]; then
    mkdir "$HOME/.vim"
    cd "$HOME/.vim"
	git clone https://github.com/flazz/vim-colorschemes.git

    cd $HOME
	echo "set ts=4" >> .vimrc
	echo "colorscheme elflord" >> .vimrc
fi

logout

chown -R $NAME:$NAME $HOME

cd $PWD